A command-line interface to <http://www.goodreads.com>, written in
python. So far all it does is print a user's most recently added 200
books (title and author), or the most recently added 200 books from one
of a user's shelves. Books are printed one per line, suitable for piping
to grep, ack, etc. You have to supply a goodreads user id and your dev
key as options:

    ~ > gr -u '3857302' -k 'xxxxxxxxxxxxxxxxxxxxxx' to-read
    Underground: Tales of hacking, madness, and obsession on the electronic 
    frontier [Suelette Dreyfus]
    The Martians [Kim Stanley Robinson]
    Blue Mars (Mars Trilogy 3) [Kim Stanley Robinson]
    Where Mathematics Comes From: How the Embodied Mind Brings Mathematics 
    into Being [George Lakoff]
    The Case for Mars [Robert Zubrin]
    The Black Cloud [Fred Hoyle]
    The Moon Is a Harsh Mistress [Robert A. Heinlein]
    VALIS [Philip K. Dick]
    Flow My Tears, the Policeman Said (SF Masterworks, #46) [Philip K. Dick]
    The Three Stigmata of Palmer Eldritch [Philip K. Dick]
    Counter-Clock World [Philip K. Dick]
    The Transmigration of Timothy Archer [Philip K. Dick]
    The World Jones Made [Philip K. Dick]
    Confederacy of Dunces, a [John Kennedy Toole]
    (14 books)
    ~ > 

It could be extended to do a lot more using the [goodreads
api](http://www.goodreads.com/api): searching for a book on a user's
shelves or on all of goodreads, getting information about authors and
lists of books by an author, printing out reviews of a book, even adding
books to shelves and posting reviews.
