#!/usr/bin/env python
import urllib,urllib2
from xml.etree import ElementTree

def get_books(user_id,dev_key,shelf):
    url_base = "http://www.goodreads.com/review/list"
    params = {'id':user_id,
              'key':dev_key,
              'v':'2',
              'format':'xml',
              'sort':'date_added',
              'per_page':'200'}
    if shelf is not None: params['shelf'] = shelf
    encoded_params = urllib.urlencode(params)
    url = url_base + '?' + encoded_params
    response = urllib2.urlopen(url)

    books = []
    tree = ElementTree.parse(response)
    for review in tree.getiterator('review'):
        for book in review.findall('book'):
            title_nodes = book.findall('title')
            assert len(title_nodes) == 1
            title = title_nodes[0].text.strip()
            authors = []
            authors_nodes = book.findall('authors')
            for authors_node in authors_nodes:
                author_nodes = authors_node.findall('author')
                for author_node in author_nodes:
                    names = author_node.findall('name')
                    assert len(names) == 1
                    authors.append(names[0].text.strip())
            authors = '[' + ', '.join([author.strip() for author in authors]) + ']'
            books.append(title + ' ' + authors)

    books.reverse() # Print most-recently-added last.
    for book in books:
        print book.encode('UTF-8')
    print "(%i books)" % len(books)

if __name__ == "__main__":
    import optparse,sys
    usage = "usage: %prog [options] [shelf]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option('-u','--user',dest='user_id',help="your goodreads.com user id")
    parser.add_option('-k','--key',dest='dev_key',help="your goodreads.com dev key")
    options,remainder = parser.parse_args()
    if options.user_id is None:
        parser.print_help()
        sys.exit(1)
    if options.dev_key is None:
        parser.print_help()
        sys.exit(1)
    if len(remainder) > 0:
        shelf = remainder[0]
    else:
        shelf = None
    get_books(options.user_id,options.dev_key,shelf)
